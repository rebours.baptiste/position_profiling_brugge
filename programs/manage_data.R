
      #######################################################################################################
      ##############           MANIPULATION ON DATA TO MAKE DATAFRAMES FOR PROFILING           ############## 
      #######################################################################################################
                                         

##############  Not necessary to run   / ! \  ##############   
      
      
### PLAYERS
players <- data.frame()
for (file in list.files("ArchiveJames3/Players",full.names=T)){
  players <- rbind(players,as_data_frame(flatten(stream_in(file(file)))))
}
players <- players %>% 
  dplyr::select(c("wyId","shortName","height","weight","birthDate","foot","currentTeamId","imageDataURL","passportArea.name","role.code2")) %>%
  rename("player_wyId"="wyId","player_imageDataURL"="imageDataURL","team_wyId"="currentTeamId") %>%
  mutate(player_wyId = as.character(player_wyId),team_wyId = as.character(team_wyId))


### TEAMS
teams <- data.frame()
for (file in list.files("ArchiveJames3/Teams",full.names=T)){
  teams <- rbind(teams,as_data_frame(flatten(stream_in(file(file)))))
}
teams <- teams %>%
  dplyr::select(c("wyId","officialName","imageDataURL")) %>%
  rename("team_wyId" = "wyId","team_imageDataURL"="imageDataURL") %>%
  mutate(team_wyId = as.character(team_wyId))


### STATS
stats_jupiler <- as_data_frame(flatten(stream_in(file("PlayerStatistics_Jupiler.json"))))
stats_jupiler <- stats_jupiler %>% 
  dplyr::select(c("playerId","competitionId","seasonId","positions","total.matches","total.minutesTagged")|starts_with("cb")|starts_with("average")|starts_with("percent")) %>%
  rename("player_wyId"="playerId","compet_wyId"="competitionId","season_wyId"="seasonId") %>%
  mutate(player_wyId = as.character(player_wyId),compet_wyId=as.character(compet_wyId),season_wyId=as.character(season_wyId))


### MERGE ###
players <- players %>% 
  left_join(teams)

player_stats_jupiler <- stats_jupiler %>% 
  left_join(players)




# Get all existing positions
exist_positions <- c()
for (i in 1:nrow(player_stats_jupiler)){
  if (nrow(player_stats_jupiler[i,]$positions[[1]])!=0){
    for (j in 1:nrow(player_stats_jupiler[i,]$positions[[1]])){
      if (!(player_stats_jupiler[i,]$positions[[1]][j,1][1] %in% exist_positions)){
        exist_positions <- c(exist_positions,player_stats_jupiler[i,]$positions[[1]][j,1][1]$name)
      }
    }
  }
}



# Give a final position for every existing position
final_positions <- c("Centre back","Centre back","Centre back","Centre back","Winger","Winger","Winger","Full back","Full back","Full back","Full back","Attacking midfielder",
                     "Striker","Winger","Winger","Defensive midfielder","Central midfielder","Defensive midfielder","Full back","Central midfielder","Centre back",
                     "Goalkeeper","Defensive midfielder","Winger","Full back")

linked_positions <- data.frame(exist_positions,final_positions)


# Change from existing position to final position
for (i in 1:nrow(player_stats_jupiler)){
  if (nrow(player_stats_jupiler[i,]$positions[[1]])!=0){
    for (j in 1:nrow(player_stats_jupiler[i,]$positions[[1]])){
      player_stats_jupiler[i,]$positions[[1]]$position[j,1] <- as.character(linked_positions[which(linked_positions[,1]==player_stats_jupiler[i,]$positions[[1]][j,1][1]$name),2])
    }
  }
}

# Summarize percent for each final position
for (i in 1:nrow(player_stats_jupiler)){
  if (nrow(player_stats_jupiler[i,]$positions[[1]])!=0){
    player_stats_jupiler[i,]$positions[[1]] <- player_stats_jupiler[i,]$positions[[1]] %>%
      group_by(position$name) %>%
      summarise(sum_percent=sum(percent)) %>%
      filter(sum_percent == max(sum_percent)) %>%
      filter(row_number() == 1)
  }
}

# Create new variable for position
pos <- c()
pos_percent <- c()
for (i in 1:nrow(player_stats_jupiler)){
  if (nrow(player_stats_jupiler[i,]$positions[[1]])!=0){
    pos <- c(pos,player_stats_jupiler[i,]$positions[[1]]$`position$name`)
    pos_percent <- c(pos_percent,player_stats_jupiler[i,]$positions[[1]]$sum_percent)
  } else {
    pos <- c(pos,NA)
    pos_percent <- c(pos_percent,NA)
  }
}
player_stats_jupiler$position <- pos
player_stats_jupiler$position_percent <- pos_percent
player_stats_jupiler <- player_stats_jupiler %>%
  dplyr::select(-positions)


# Keeping only 3 previous seasons
player_stats_jupiler <- player_stats_jupiler %>%
  filter(season_wyId %in% c("185753","185569","181161"))
player_stats_jupiler$season_name <- ifelse(player_stats_jupiler$season_wyId=="185753","2019/20",ifelse(player_stats_jupiler$season_wyId=="185569","2018/19","2017/18"))





### Dataframe for profiling per position
# Keep 19/20 season
# Remove players who didn't play at least 10% of total minutes played
# Remove players without position noticed
# Remove players that did not play at least 50% at the same position

player_stats_jupiler_1920 <- player_stats_jupiler %>%
  filter(season_wyId == "185753") %>%
  filter(!is.na(position)) %>%
  filter(total.minutesTagged >= 0.1*34*90) %>%
  filter(position_percent >= 50) %>%
  mutate(state = ifelse(total.minutesTagged >= 0.2*34*90 & position_percent >= 75,"actif","supp")) %>%
  column_to_rownames("player_wyId")



### Dataframe for position profiling
# Only players who played at least 20% of total minutes played and 75% at the same position (active)
# No GK
# Remove GK stats
# Remove correlated variables

player_stats_jupiler_1920_active <- player_stats_jupiler_1920 %>%
  filter(position != "Goalkeeper") %>%
  filter(state=="actif") %>%
  dplyr::select(position,average.passLength:percent.dribblesAgainstWon) %>%
  dplyr::select(!starts_with("average.gk") & !starts_with("percent.gk")) %>%
  dplyr::select(-average.goalKicksShort,-average.successfulGoalKicks,-percent.successfulGoalKicks,-average.goalKicksLong,-average.xgSave,-average.goalKicks) %>% #gk variables
  dplyr::select(-average.shotOnTargetAssists,-average.fieldAerialDuels,-percent.fieldAerialDuelsWon,-average.lateralPasses,-average.successfulLateralPasses,-percent.successfulLateralPasses,-average.successfulBackPasses,-average.successfulKeyPasses,-average.successfulVerticalPasses) %>% #cor ~= 1
  mutate(position=factor(position,levels=c("Centre back","Full back","Defensive midfielder","Central midfielder","Winger","Striker")))


