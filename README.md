# position_profiling_brugge

## Project description
### Player profiling - Detection of profiles for each position
- Which statistics are ‘typical’ for each position
- Which statistics differentiate each profile from other profiles for the same position

### Stability of the profile – Visualization of the evolution of statistics
- How do the statistics specific to each position/profile evolve over the seasons
- Are there differences depending on whether the player plays in a particular competition




## Order to run programs:
1. param.R
2. load_data.R
3. profiling_positions.R
4. profiling_cb.R


## Programs contents:
- param.R : packages to load
- load_data.R : dataframes to load to run profiling programs
- profiling_positions.R : position profiling / LDA
- profiling_cb.R : profile per position (ex: CB) / ACP + Kmeans
- manage_data.R : how dataframes were built